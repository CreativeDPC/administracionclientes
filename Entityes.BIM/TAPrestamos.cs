//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Entityes.BIM
{
    using System;
    using System.Collections.Generic;
    using Entityes.BIM;
    using DataAcces.BIM;
    using System.Runtime.Serialization;

    [DataContract]
    public partial class TAPrestamos
    {
        [DataMember]
        public int FIPrestamoID { get; set; }
        [DataMember]
        public int FIAcreedorID { get; set; }
        [DataMember]
        public int FIDeudorID { get; set; }
        [DataMember]
        public decimal FNMonto { get; set; }
        [DataMember]
        public int FINumeroPeriodos { get; set; }
        [DataMember]
        public decimal FNTasa { get; set; }
        [DataMember]
        public System.DateTime FDFechaSolicitud { get; set; }
        [DataMember]
        public System.DateTime FDFechaAlta { get; set; }

        [DataMember]
        public virtual TAAcreedores TAAcreedores { get; set; }
        [DataMember]
        public virtual TADeudores TADeudores { get; set; }
    }
}
