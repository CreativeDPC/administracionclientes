﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entityes.BIM;
using DataAcces.BIM;

namespace BusinessLogic.BIM
{
    public class PrestamoBusinessBIM
    {
        static PrestamoDataAccessBIM PrestamoDatos = new PrestamoDataAccessBIM();

        public static List<TAPrestamos> ObtenerPrestamos()
        {
            return PrestamoDatos.GetPrestamos();
        }
    }
}
