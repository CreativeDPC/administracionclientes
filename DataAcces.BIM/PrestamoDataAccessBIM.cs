﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entityes.BIM;

namespace DataAcces.BIM
{
    public class PrestamoDataAccessBIM
    {
        PrestamosBIMEntities context;

        public List<TAPrestamos> GetPrestamos()
        {
            using (context = new PrestamosBIMEntities())
            {
                var q = from c in context.TAPrestamos
                        select c;

                return q.ToList();
            }
            
        }
        
    }
}
